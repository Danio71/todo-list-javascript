let tasksArray = [];
let text, button;

const addTask = () => {
	let taskName = document.getElementById("createNewTask").value;
	
	if(!taskName) console.log("Nic nie wprowadzono!");	// !taskName - empty = true OR taskName == ''
	else {
		tasksArray[tasksArray.length] = taskName;
		
		document.querySelector(".allTasks").innerHTML += 
		` <div class="task" id="${tasksArray.length - 1}">
		
			<label class="container">
				<input type="checkbox" id="i${tasksArray.length - 1}" onclick="moveTask(${tasksArray.length - 1})">
				<span class="checkmark"> </span>
			</label> <a class="taskTitle"> ${taskName} </a>
			
			<div class="modify">
				<button class="options" onclick="editTask(${tasksArray.length - 1})"> &#x270E; </button>
				<button class="options" onclick="deleteTask(${tasksArray.length - 1})"> &#xd7; </button>
			</div>
		 </div> `
	}
	
	clearInput();
}

const editTask = (id) => {
	
	text = document.getElementById(`${id}`).getElementsByTagName('a')[0]; //document.querySelector('#\\30 ').innerHTML = '';
	button = document.getElementById(`${id}`).getElementsByClassName(`options`)[0];	
	
	button.setAttribute(`onclick`, `updateTask(${id})`);
	text.innerHTML = `<input id="updateTask" type="text" value="${tasksArray[id]}">`;
}

const updateTask = (id) => {
	
	let taskName = document.getElementById("updateTask").value;
	
	let inputDelete = document.getElementById('updateTask');
	inputDelete.parentNode.removeChild(inputDelete);
	
	tasksArray[id] = taskName;
	
	text.innerHTML = tasksArray[id];
	
	button.setAttribute(`onclick`, `editTask(${id})`);
}

const deleteTask = (id) => {
	let task = document.getElementById(`${id}`);
	task.parentNode.removeChild(task);
}

const moveTask = (id) => {
	let task = document.getElementById(`${id}`);
	let isChecked = document.getElementById(`i${id}`).checked;
	
	if(isChecked) document.querySelector(".completedTasks").appendChild(task);
	else document.querySelector(".allTasks").appendChild(task);
}

const clearInput = () => {
	document.getElementById("createNewTask").value = '';
}

window.addEventListener('keypress', (key) => {
	if(key.keyCode == 13) addTask();
});